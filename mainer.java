

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import javax.xml.soap.Text;











public class mainer { 
	
	public static void main(String[] args) throws IOException {
	
		String katalog = "/home/msi/Pulpit/Test1/"; // Sciezka do katalogu 
		long wordLimit = 10; // Ograniczenie posegregowanych nazw do 10
		final AtomicBoolean endOfWork = new AtomicBoolean(false); //Zmienna pozwalająca zatrzymać działanie wątków
		final int liczbaProducentów = 1;
		final int liczbaKonsumentów = 2;
		final ExecutorService wykonawca = Executors.newFixedThreadPool(liczbaProducentów + 
		liczbaKonsumentów); //Tworzenie 3 wątków, 1 dla producenta, 2 dla konsumentów
		final BlockingQueue<Optional<Path>> kolejka = new LinkedBlockingDeque<>(3);
		//Kolejka 2 elementowa
		Runnable producent = () -> {
			String name = Thread.currentThread().getName(); //Pobieranie "nazwy" wątku.
			System.out.println("PRODUCENT " + name + " URUCHOMIONY ..."); //Wypisywanie informacji o rozpoczęciu pracy wątku.
			listing(katalog, kolejka); //Tworzenie listy plików i wkładanie ich do kolejki.
			endOfWork.set(true); //Informacja dla innych wątków o skończonej pracy.
			System.out.println("PRODUCENT " + name + " ZAKOŃCZONY."); //Wypisanie informacji o zakończeniu pracy.
		
		
		

};

Runnable konsument = () -> { 
	String name = Thread.currentThread().getName(); //Pobieranie "nazwy" wątku.
	System.out.println("KONSUMENT " + name + " URUCHOMIONY ..."); //Wypisywanie informacji o rozpoczęciu pracy wątku.
							while (true) { 
								try {
									if (!kolejka.isEmpty()) { //Sprawdzenie czy kolejka nie jest pusta
										searchingWords(kolejka.take(), wordLimit);
									} else {
										if (endOfWork.compareAndSet(true, true)) {
											System.out.print( "KONSUMENT " + name + "ZAKOŃCZONY");
													break;
										
										
										}
									}
										
										
										
								} catch (InterruptedException e1) {		
									e1.printStackTrace();
								}
									
								}
									
									
									
									
									
						}; 
								
								for (int i = 0; i < liczbaProducentów; i++) //Uruchomienie wszystkich wątków
									
									
									wykonawca.execute(producent); 
								for (int i = 0; i < liczbaKonsumentów; i++); 
									wykonawca.shutdown(); //Inizjalizuje uporządkowane zamknięcie, nowe zadanie nie
									
									try {
										TimeUnit.SECONDS.sleep(15);
									} 
									catch (InterruptedException e) {
									}
									endOfWork.set(true); //Zakończenie pracy wątków. 
									
									System.out.println("Koniec programu.");
									System.exit(0);
									
									
	}
									
									
									public static void listing(String directoryName, BlockingQueue<Optional<Path>> kolejka) {
									
										File directory = new File(directoryName);
										File[] fList = directory.listFiles();
										
										boolean pelna = false;
										if (fList != null)
											for (File file : fList) {
												do {
													if (file.getName().endsWith(".txt")) {
													Optional<Path> optPath =
													Optional.ofNullable(file.toPath());
													try {
														
														kolejka.add(optPath);
														pelna = false;
													}
													
													catch (IllegalStateException e) {
														pelna = true; //Nie można było dodać pliku do kolejki, więc była pełna.
														}
														}
														
														} while (pelna);
														
									}
							}
				
														
		private static void searchingWords(Optional<Path> path, Long wordLimit) {
			String textLinia; 
			Path sciezka = path.get();
			String textRead = "";
			Map<String, Long> map = new LinkedHashMap<>(); 
			try (BufferedReader br = Files.newBufferedReader(sciezka.toAbsolutePath(),
					StandardCharsets.UTF_8)) 
			{
				while ((textLinia = br.readLine()) !=null)
				{
					textRead += " " + textLinia; 
					
				}
				String textToLower = textRead.toLowerCase();
				String textRegex = textToLower.replaceAll ("[^a-zążćśńłóęż]", " " );
				
				String [] words = textRegex.split(" ", 0);
				for (String word : words) 
				{
					if (word.length() >2) 
					{
						if (map.containsKey(word)) {
							map.put(word, map.get(word) +1 );
							
						}
						else 
						{
							map.put(word, 1l); 
						}
					}
				}
				
				
				
			}
			catch (Exception e) 
			{
				e.printStackTrace(); 
				
			}
			
			Map<String, Long> mapSortDSC = map.entrySet().stream().
					sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).limit(wordLimit).
					collect(Collectors.toMap(
							Map.Entry::getKey, Map.Entry::getValue, 
							(oldValue, newValue) -> oldValue, LinkedHashMap::new ));
			System.out.print("Sciezka pliku.txt --> " + sciezka +  " \nWyrazy nieposortowane: " + map 
					+ " \nWyrazy posortowane: " + mapSortDSC); 					
					
				
		}
	}
			
			
					
			
			
			
			
			
			
			
			
			
		
											
										
										
				
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
